#include "stdafx.h"
#include "MathHelper.h"

namespace S2D
{
	namespace MathHelper
	{
		const float MathHelper::E = 2.71828175f;
		const float MathHelper::Log10E = 0.4342945f;
		const float MathHelper::Log2E = 1.442695f;
		const float MathHelper::Pi = 3.14159274f;
		const float MathHelper::PiOver2 = 1.57079637f;
		const float MathHelper::PiOver4 = 0.7853982f;
		const float MathHelper::TwoPi = 6.28318548f;

		float MathHelper::Clamp(float value, float min, float max)
		{
			if (value >= max)
				return max;
			else if (value <= min)
				return min;
			else
				return value;
		}

		float MathHelper::Distance(float value1, float value2)
		{
			return abs(value1 - value2);
		}

		float MathHelper::Lerp(float value1, float value2, float amount)
		{
			if (amount <= 0.0f)
				return value1;
			else if (amount >= 1.0f)
				return value2;
			else
				return value1 + (value2 - value1) * amount;
		}

		float MathHelper::Max(float value1, float value2)
		{
			if (value1 >= value2)
				return value1;
			else
				return value2;
		}

		float MathHelper::Min(float value1, float value2)
		{
			if (value1 <= value2)
				return value1;
			else
				return value2;
		}

		int MathHelper::Max(int value1, int value2)
		{
			if (value1 >= value2)
				return value1;
			else
				return value2;
		}

		int MathHelper::Min(int value1, int value2)
		{
			if (value1 <= value2)
				return value1;
			else
				return value2;
		}

		float MathHelper::ToDegrees(float radians)
		{
			return radians * (180 / Pi);
		}

		float MathHelper::ToRadians(float degrees)
		{
			return degrees * (Pi / 180);
		}

		float MathHelper::WrapAngle(float angle)
		{
			int x = (int)(angle / Pi);
			
			x = x - (x % 2);

			angle -= (x * Pi);

			if (angle > Pi)
				return (angle - Pi) * -1;
			else if (angle < -Pi)
				return (angle + Pi) * -1;
			else
				return angle;
		}

		float MathHelper::Round(float value)
		{
			return value < 0.0f ? ceilf(value - 0.5f) : floorf(value + 0.5f);
		}
	}
}
