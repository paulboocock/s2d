#include "Stdafx.h"
#include "Texture2D.h"

#include <fstream>
#include <iostream>

using namespace std;

namespace S2D
{
	Texture2D::Texture2D()	
	{
		_ID = new GLuint;
	}

	Texture2D::~Texture2D()
	{
		glDeleteTextures(1, _ID);
		delete _ID;
	}

	void Texture2D::LoadRAW(const char* textureFileName, int width, int height, bool buildMipMaps)
	{
		char* tempTextureData;  
		int fileSize;
		ifstream inFile;

		_width = width;
		_height = height;

		inFile.open(textureFileName, ios::binary);

		if (!inFile.good())  
		{
#if _DEBUG
			cerr  << "Can't open texture file " << textureFileName << endl;
#endif
			return;
		}

		inFile.seekg (0, ios::end); //Seek to end of file
		fileSize = (int)inFile.tellg(); //Get current position in file - The End, this gives us total file size
		tempTextureData = new char [fileSize]; //Create an new aray to store data
		inFile.seekg (0, ios::beg); //Seek back to beginning of file
		inFile.read (tempTextureData, fileSize); //Read in all the data in one go
		inFile.close(); //Close the file

#if _DEBUG
		cout << textureFileName << " loaded." << endl;
#endif

		glGenTextures(1, _ID); //Get next Texture ID
		glBindTexture(GL_TEXTURE_2D, *_ID); //Bind the texture to the ID

		if (buildMipMaps)
		{
			gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, GL_UNSIGNED_BYTE, tempTextureData);
		}
		else
		{
			glTexImage2D(GL_TEXTURE_2D, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, tempTextureData);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}		

		delete [] tempTextureData; //Clear up the data - We don't need this any more
	}

	void Texture2D::Load(const char* textureFileName, bool buildMipMaps)
	{ 
		int channels;

		unsigned char* data = SOIL_load_image
		(
			textureFileName,
			&_width, &_height, &channels,
			SOIL_LOAD_AUTO
		);

		glGenTextures(1, _ID); //Get next Texture ID
		glBindTexture(GL_TEXTURE_2D, *_ID); //Bind the texture to the ID

		if (buildMipMaps)
		{
			//Note that TGA files are stored as BGR(A) - So we need to specify the format as GL_BGR(A)_EXT
			if (channels == 4)
				gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, _width, _height, GL_RGBA, GL_UNSIGNED_BYTE, data);
			else
				gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, _width, _height, GL_RGB, GL_UNSIGNED_BYTE, data);
		}
		else
		{
			//Note that TGA files are stored as BGR(A) - So we need to specify the format as GL_BGR(A)_EXT
			if (channels == 4)
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
			else
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, _width, _height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}

		delete [] data;
	}

	GLuint * Texture2D::GetID() const
	{
		return _ID;
	}

	int Texture2D::GetWidth() const
	{
		return _width;
	}

	int Texture2D::GetHeight() const
	{
		return _height;
	}

	void Texture2D::GetData(Color* data) const
	{
		unsigned char * bytes = new unsigned char[_width * _height * 4];

		glBindTexture(GL_TEXTURE_2D, *_ID);

		glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, bytes);

		int byteWidth = _width * 4;

		for (int i = 0; i < _width; i++)
		{
			for (int j = 0; j < _height; j++)
			{
				int bytePos = (i * 4) + (j * byteWidth);

				data[i*j].R = bytes[bytePos] / 255.0f;
				data[i*j].G = bytes[bytePos + 1] / 255.0f;
				data[i*j].B = bytes[bytePos + 2] / 255.0f;
				data[i*j].A = bytes[bytePos + 3] / 255.0f;
			}
		}

		delete [] bytes;
	}
}