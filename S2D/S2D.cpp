#include "stdafx.h"
#include "S2D.h"

#include <iostream>
#include <stdio.h>

namespace S2D
{
	const Color* Color::Black = new Color();
	const Color* Color::White = new Color(1.0f, 1.0f, 1.0f);
	const Color* Color::Red = new Color(1.0f, 0.0f, 0.0f);
	const Color* Color::Green = new Color(0.0f, 1.0f, 0.0f);
	const Color* Color::Blue = new Color(0.0f, 0.0f, 1.0f);
	const Color* Color::Yellow = new Color(1.0f, 1.0f, 0.0f);
	const Color* Color::Cyan = new Color(0.0f, 1.0f, 1.0f);
	const Color* Color::Magenta = new Color(1.0f, 0.0f, 1.0f);
	const Rect* Rect::Empty = new Rect();

	Color::Color()
	{
		R = 0; G = 0; B = 0; A = 1;
	}

	Color::Color(float r, float g, float b)
	{
		this->R = r; this->G = g; this->B = b; this->A = 1;
	}

	Color::Color(float r, float g, float b, float a)
	{
		this->R = r; this->G = g; this->B = b; this->A = a;
	}

	bool Color::operator== (const Color &rhs) const
	{
		if (this->R == rhs.R && this->G == rhs.G && this->B == rhs.B && this->A == rhs.A)
			return true;
		else
			return false;
	}

	bool Color::operator!=(const Color &other) const
	{
		return !(*this == other);
	}

	Color & Color::operator= (const Color &rhs)
	{
		this->R = rhs.R;
		this->G = rhs.G;
		this->B = rhs.B;
		this->A = rhs.A;
		return *this;
	}

	Rect::Rect()
	{
		X = 0.0f; Y = 0.0f;
		Width = 0; Height = 0;
	}

	Rect::~Rect()
	{}

	Rect::Rect(float x, float y, int width, int height)
	{ 
		this->X = x; this->Y = y; 
		this->Width = width; this->Height = height; 
	}

	bool Rect::operator== (const Rect &rhs) const
	{
		if (this->X == rhs.X && this->Y == rhs.Y
			&& this->Width == rhs.Width && this->Height == rhs.Height)
			return true;
		else
			return false;
	}

	bool Rect::operator!=(const Rect &other) const
	{
		return !(*this == other);
	}

	Rect & Rect::operator= (const Rect &rhs)
	{
		this->X = rhs.X;
		this->Y = rhs.Y;
		this->Width = rhs.Width;
		this->Height = rhs.Height;
		return *this;
	}

	Vector2 Rect::Center() const
	{
		Vector2 result;

		result.X = X + ((float)Width / 2.0f);
		result.Y = Y + ((float)Height / 2.0f);

		return result;
	}

	bool Rect::IsEmpty() const
	{
		if (X != 0.0f || Y != 0.0f || Width != 0 || Height != 0)
			return false;
		else
			return true;
	}

	float Rect::Top() const
	{
		return Y;
	}

	float Rect::Bottom() const
	{
		return Y + Height;
	}

	float Rect::Left() const
	{
		return X;
	}

	float Rect::Right() const
	{
		return X + Width;
	}

	bool Rect::Intersects(const Rect& rect) const
	{
		return (Left() < rect.Right()
			&& Right() > rect.Left()
			&& Top() < rect.Bottom()
			&& Bottom() > rect.Top());
	}

	void Rect::Intersects(const Rect& rect, bool& result) const
	{
		result = (Left() < rect.Right()
			&& Right() > rect.Left()
			&& Top() < rect.Bottom()
			&& Bottom() > rect.Top());
	}

	bool Rect::Contains(const Vector2& vector) const
	{
		return (Left() < vector.X
			&& Right() > vector.X
			&& Top() < vector.Y
			&& Bottom() > vector.Y);
	}

	void Rect::Contains(const Vector2& vector, bool& result) const
	{
		result = (Left() < vector.X
			&& Right() > vector.X
			&& Top() < vector.Y
			&& Bottom() > vector.Y);
	}

	const Vector2* Vector2::Zero = new Vector2();
	const Vector2* Vector2::One = new Vector2(1.0f, 1.0f);
	const Vector2* Vector2::UnitX = new Vector2(1.0f, 0.0f);
	const Vector2* Vector2::UnitY = new Vector2(1.0f, 0.0f);

	Vector2::Vector2()
	{
		X = 0; Y = 0;
	}

	Vector2::Vector2(float x, float y)
	{ 
		this->X = x; this->Y = y; 
	}

	bool Vector2::operator== (const Vector2 &rhs) const
	{
		if (this->X == rhs.X && this->Y == rhs.Y)
			return true;
		else
			return false;
	}

	bool Vector2::operator!=(const Vector2 &other) const
	{
		return !(*this == other);
	}

	Vector2 & Vector2::operator/=(const Vector2 &rhs)
	{
		this->X /= rhs.X;
		this->Y /= rhs.Y;

		return *this;
	}

	Vector2 & Vector2::operator*=(const Vector2 &rhs)
	{
		this->X *= rhs.X;
		this->Y *= rhs.Y;

		return *this;
	}

	Vector2 & Vector2::operator/=(const float &rhs)
	{
		this->X /= rhs;
		this->Y /= rhs;

		return *this;
	}

	Vector2 & Vector2::operator*=(const float &rhs)
	{
		this->X *= rhs;
		this->Y *= rhs;

		return *this;
	}

	Vector2 & Vector2::operator= (const Vector2 &rhs)
	{
		this->X = rhs.X;
		this->Y = rhs.Y;
		return *this;
	}

	Vector2 & Vector2::operator+=(const Vector2 &rhs)
	{
		this->X += rhs.X;
		this->Y += rhs.Y;

		return *this;
	}

	Vector2 & Vector2::operator-=(const Vector2 &rhs)
	{
		this->X -= rhs.X;
		this->Y -= rhs.Y;

		return *this;
	}

	const Vector2 Vector2::operator+(const Vector2 &other) const
	{
		Vector2 result = *this;
		result += other;
		return result;
	}

	const Vector2 Vector2::operator-(const Vector2 &other) const
	{
		Vector2 result = *this;
		result -= other;
		return result;
	}

	const Vector2 Vector2::operator*(const Vector2 &other) const
	{
		Vector2 result = *this;
		result *= other;
		return result;
	}

	const Vector2 Vector2::operator/(const Vector2 &other) const
	{
		Vector2 result = *this;
		result /= other;
		return result;
	}

	const Vector2 Vector2::operator*(const float &other) const
	{
		Vector2 result = *this;
		result *= other;
		return result;
	}
		
	const Vector2 Vector2::operator/(const float &other) const
	{
		Vector2 result = *this;
		result /= other;
		return result;
	}

	float Vector2::Length() const
	{
		return sqrtf(pow(this->X, 2) + pow(this->Y, 2));
	}

	float Vector2::LengthSquared() const
	{
		return pow(this->X, 2) + pow(this->Y, 2);
	}

	void Vector2::Normalize()
	{
		float length = this->Length();
		this->X /= length;
		this->Y /= length;
	}

	Vector2 Vector2::Clamp(const Vector2& value1, const Vector2& min, const Vector2& max)
	{
		Vector2 result;

		if (value1.X < min.X)
			result.X = min.X;
		if (value1.Y < min.Y)
			result.Y = min.Y;
		if (value1.X > max.X)
			result.X = max.X;
		if (value1.Y > max.Y)
			result.Y = max.Y;

		return result;
	}

	void Vector2::Clamp(const Vector2& value1, const Vector2& min, const Vector2& max, Vector2& result)
	{
		if (value1.X < min.X)
			result.X = min.X;
		if (value1.Y < min.Y)
			result.Y = min.Y;
		if (value1.X > max.X)
			result.X = max.X;
		if (value1.Y > max.Y)
			result.Y = max.Y;
	}

	float Vector2::Distance(const Vector2& value1, const Vector2& value2)
	{
		return (value1 - value2).Length();
	}

	void Vector2::Distance(const Vector2& value1, const Vector2& value2, float& result)
	{
		result = (value1 - value2).Length();
	}

	float Vector2::DistanceSquared(const Vector2& value1, const Vector2& value2)
	{
		return (value1 - value2).LengthSquared();
	}

	void Vector2::DistanceSquared(const Vector2& value1, const Vector2& value2, float& result)
	{
		result = (value1 - value2).LengthSquared();
	}

	float Vector2::Dot(const Vector2& value1, const Vector2& value2)
	{
		return (value1.X * value2.X) + (value1.Y * value2.Y);
	}

	void Vector2::Dot(const Vector2& value1, const Vector2& value2, float& result)
	{
		result = (value1.X * value2.X) + (value1.Y * value2.Y);
	}

	Vector2 Vector2::Lerp(const Vector2& value1, const Vector2& value2, const float amount)
	{
		return value1 + (value2 - value1) * amount;
	}

	void Vector2::Lerp(const Vector2& value1, const Vector2& value2, const float amount, Vector2& result)
	{
		result = value1 + (value2 - value1) * amount;
	}

	Vector2 Vector2::Max(const Vector2& value1, const Vector2& value2)
	{
		Vector2 result;

		result.X = value1.X > value2.X ? value1.X : value2.X;
		result.Y = value1.Y > value2.Y ? value1.Y : value2.Y;

		return result;
	}

	void Vector2::Max(const Vector2& value1, const Vector2& value2, Vector2& result)
	{
		result.X = value1.X > value2.X ? value1.X : value2.X;
		result.Y = value1.Y > value2.Y ? value1.Y : value2.Y;
	}

	Vector2 Vector2::Min(const Vector2& value1, const Vector2& value2)
	{
		Vector2 result;

		result.X = value1.X < value2.X ? value1.X : value2.X;
		result.Y = value1.Y < value2.Y ? value1.Y : value2.Y;

		return result;
	}

	void Vector2::Min(const Vector2& value1, const Vector2& value2, Vector2& result)
	{
		result.X = value1.X < value2.X ? value1.X : value2.X;
		result.Y = value1.Y < value2.Y ? value1.Y : value2.Y;
	}

	Vector2 Vector2::Negate(const Vector2& value)
	{
		return Vector2(-value.X, -value.Y);
	}

	void Vector2::Negate(const Vector2& value, Vector2& result)
	{
		result.X = -value.X;
		result.Y = -value.Y;
	}

	Vector2 Vector2::Normalize(const Vector2& value)
	{
		Vector2 result;
		float length = value.Length();
		result.X = value.X / length;
		result.Y = value.Y / length;
		return result;
	}

	void Vector2::Normalize(const Vector2& value, Vector2& result)
	{
		float length = value.Length();
		result.X = value.X / length;
		result.Y = value.Y / length;
	}

	Vector2 Vector2::Reflect(const Vector2& vector, const Vector2& normal)
	{
		//R = V - 2 * (V . N) * N
		Vector2 result;
		float temp;
		Vector2::Dot(vector, normal, temp);

		result = vector - normal * temp * 2;

		return result;
	}

	void Vector2::Reflect(const Vector2& vector, const Vector2& normal, Vector2& result)
	{
		float temp;
		Vector2::Dot(vector, normal, temp);

		result = vector - normal * temp * 2;
	}

}