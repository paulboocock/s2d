========================================================================
    DYNAMIC LINK LIBRARY : S2D Project Overview
========================================================================

Acknowledgments:
http://freeglut.sourceforge.net/
http://www.transmissionzero.co.uk/software/freeglut-devel/

/////////////////////////////////////////////////////////////////////////////

This file contains a summary of what you will find in each of the files that
make up your S2D application.


S2D.vcxproj
    This is the main project file for VC++ projects generated using an Application Wizard.
    It contains information about the version of Visual C++ that generated the file, and
    information about the platforms, configurations, and project features selected with the
    Application Wizard.

S2D.vcxproj.filters
    This is the filters file for VC++ projects generated using an Application Wizard. 
    It contains information about the association between the files in your project 
    and the filters. This association is used in the IDE to show grouping of files with
    similar extensions under a specific node (for e.g. ".cpp" files are associated with the
    "Source Files" filter).

S2D.cpp
    This is the main DLL source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named S2D.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////

Other notes:

/////////////////////////////////////////////////////////////////////////////
