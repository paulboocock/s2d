#include "stdafx.h"
#include "SpriteBatch.h"
#include "Graphics.h"

namespace S2D
{
	namespace SpriteBatch
	{
		void BeginDraw()
		{
			glMatrixMode(GL_MODELVIEW);
			glClear(GL_COLOR_BUFFER_BIT);
		}

		void EndDraw()
		{
			glFlush();
			glutSwapBuffers();
		}

		void DrawRectangle(float x, float y, int width, int height, const Color* color)
		{
			glPushMatrix();
			glDisable(GL_TEXTURE_2D);
			glColor4f(color->R, color->G, color->B, color->A);
			glBegin(GL_QUADS);
				glVertex2f(x, y);
				glVertex2f(x + width, y);
				glVertex2f(x + width, y + height);
				glVertex2f(x, y + height);
			glEnd();
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glEnable(GL_TEXTURE_2D);
			glPopMatrix();
		}

		void DrawRectangle(const Vector2* position, int width, int height, const Color* color)
		{
			DrawRectangle(position->X, position->Y, width, height, color);
		}

		void DrawRectangle(const Rect* destRectangle, const Color* color)
		{
			DrawRectangle(destRectangle->X, destRectangle->Y, destRectangle->Width, destRectangle->Height, color);
		}

		void Draw(const Texture2D* texture, const Rect* destRectangle, const Rect* sourceRectangle, const Vector2* origin, float scale, float rotation, const Color* color, SpriteEffect spriteEffect)
		{
			//Calculate TexCoords
			float x1, x2, y1, y2;

			if (sourceRectangle == nullptr) //Can be null - Draws entire texture
			{
				if (spriteEffect == SpriteEffect::NONE || spriteEffect == SpriteEffect::FLIPVERTICAL)
				{
					x1 = 0.0f; x2 = 1.0f; 
				}
				else
				{
					x1 = 1.0f; x2 = 0.0f; 
				}

				y1 = 0.0f; y2 = 1.0f;
			}
			else //Calculate TexCoords based on Source Rect
			{
				if (spriteEffect == SpriteEffect::NONE || spriteEffect == SpriteEffect::FLIPVERTICAL)
				{
					x1 = sourceRectangle->X / (float)texture->GetWidth();
					x2 = (sourceRectangle->X + (float)sourceRectangle->Width) / (float)texture->GetWidth();
				}
				else
				{
					x1 = (sourceRectangle->X + (float)sourceRectangle->Width) / (float)texture->GetWidth();
					x2 = sourceRectangle->X / (float)texture->GetWidth();
				}

				y1 = sourceRectangle->Y / (float)texture->GetHeight();
				y2 = (sourceRectangle->Y + (float)sourceRectangle->Height) / (float)texture->GetHeight();

			}

			if (spriteEffect == SpriteEffect::FLIPVERTICAL || spriteEffect == SpriteEffect::FLIPBOTH)
			{
				float temp = y1;
				y1 = y2;
				y2 = temp;
			}

			glColor4f(color->R, color->G, color->B, color->A);

			glBindTexture(GL_TEXTURE_2D, *(texture->GetID()));
			glPushMatrix();

			glTranslatef(destRectangle->X, destRectangle->Y, 0.0f);	//Move to desired location
			glScalef(scale, scale, 0.0f);							//Scale the sprite
			glRotatef(rotation, 0.0f, 0.0f, -1.0f);					//Perform rotation
			glTranslatef(-origin->X, -origin->Y, 0.0f);				//Translate to desired Origin

			glBegin(GL_QUADS);	//This Quad is drawn with the bottom left as the origin to match OpenGL Viewport
				
				glTexCoord2f(x1, y1);
				glVertex2f(0.0f, 0.0f);
				glTexCoord2f(x1, y2);
				glVertex2f(0.0f, (GLfloat)destRectangle->Height);
				glTexCoord2f(x2, y2);
				glVertex2f((GLfloat)destRectangle->Width, (GLfloat)destRectangle->Height);
				glTexCoord2f(x2, y1);
				glVertex2f((GLfloat)destRectangle->Width, 0.0f);
			glEnd();
			
			glPopMatrix();

			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		}

		void Draw(const Texture2D* texture, const Vector2* position)
		{
			Draw(texture, position, 1.0f, 0.0f);
		}

		void Draw(const Texture2D* texture, const Vector2* position, const Rect* sourceRectangle)
		{
			Draw(texture, position, sourceRectangle, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
		}

		void Draw(const Texture2D* texture, const Vector2* position, float scale, float rotation)
		{
			Draw(texture, position, nullptr, Vector2::Zero, scale, rotation, Color::White, SpriteEffect::NONE);
		}

		void Draw(const Texture2D* texture, const Vector2* position, const Rect* sourceRectangle, const Vector2* origin, float scale, float rotation, const Color* color, SpriteEffect spriteEffect)
		{
			Rect* destRect;

			if (sourceRectangle == nullptr)
				destRect = new Rect(position->X, position->Y, texture->GetWidth(), texture->GetHeight());
			else
				destRect = new Rect(position->X, position->Y, sourceRectangle->Width, sourceRectangle->Height);

			Draw(texture, destRect, sourceRectangle, origin, scale, rotation, color, spriteEffect);

			delete destRect;
		}

		void Draw(const Texture2D* texture, const Rect* destRectangle)
		{
			Draw(texture, destRectangle, 1.0f, 0.0f);
		}

		void Draw(const Texture2D* texture, const Rect* destRectangle, const Rect* sourceRectangle)
		{
			Draw(texture, destRectangle, sourceRectangle, Vector2::Zero, 1.0f, 0.0f, Color::White, SpriteEffect::NONE);
		}

		void Draw(const Texture2D* texture, const Rect* destRectangle, float scale, float rotation)
		{
			Draw(texture, destRectangle, nullptr, Vector2::Zero, scale, rotation, Color::White, SpriteEffect::NONE);
		}

		void DrawString(const char* text, const Vector2* position, const Color* color)
		{
			glPushMatrix();
				glDisable(GL_TEXTURE_2D);
				glColor4f(color->R, color->G, color->B, color->A);
				glRasterPos2f(position->X, position->Y);
				glutBitmapString(GLUT_BITMAP_HELVETICA_18, (unsigned char*)text);
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				glEnable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
}
