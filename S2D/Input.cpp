#include "stdafx.h"
#include "Input.h"

#include <iostream>

namespace S2D
{

	namespace Input
	{
		MouseState::MouseState()
		{
			LeftButton = ButtonState::RELEASED;
			MiddleButton = ButtonState::RELEASED;
			RightButton = ButtonState::RELEASED;
			ScrollWheelValue = 0;
			X = 0;
			Y = 0;
		}

		KeyboardState::KeyboardState()
		{}

		KeyArray::KeyArray()
		{
			Items.fill(Input::KeyState::RELEASED); //Ensure all keys are released by default
		}

		KeyState& KeyArray::operator[] (Keys key)
		{
			return Items[ static_cast< std::size_t >(key) ];
		}

		KeyState KeyArray::operator[] (Keys key) const
		{
			return Items[ static_cast< std::size_t >(key) ];
		}

		std::vector<Keys>& KeyboardState::GetPressedKeys()
		{
			PressedKeys.clear();

			for (int i = 0; i < static_cast<int>(Keys::COUNT); i++)
			{
				Keys key = static_cast<Keys>(i);

				if (Items[key] == KeyState::PRESSED)
				{
					PressedKeys.push_back(key);
				}
			}

			return PressedKeys;
		}

		bool KeyboardState::IsKeyDown(Keys key)
		{
			return (Items[key] == KeyState::PRESSED);
		}
		
		bool KeyboardState::IsKeyUp(Keys key)
		{
			return (Items[key] == KeyState::RELEASED);
		}

		namespace Mouse
		{
			namespace
			{
				MouseState* mouseState = new MouseState();
			}

			MouseState* GetState()
			{
				return mouseState;
			}

			void SetPosition(int x, int y)
			{
				glutWarpPointer(x, y);
			}

			void PassiveMouseMotion(int x, int y)
			{
				mouseState->X = x;
				mouseState->Y = y;
			}

			void MouseMotion(int x, int y)
			{
				mouseState->X = x;
				mouseState->Y = y;
			}

			void MouseAction(int button, int state, int x, int y)
			{
				switch (button)
				{
				case GLUT_LEFT_BUTTON:
					if (state == GLUT_UP)
						mouseState->LeftButton = ButtonState::RELEASED;
					else
						mouseState->LeftButton = ButtonState::PRESSED;
					break;
				case GLUT_MIDDLE_BUTTON:
					if (state == GLUT_UP)
						mouseState->MiddleButton = ButtonState::RELEASED;
					else
						mouseState->MiddleButton = ButtonState::PRESSED;
					break;
				case GLUT_RIGHT_BUTTON:
					if (state == GLUT_UP)
						mouseState->RightButton = ButtonState::RELEASED;
					else
						mouseState->RightButton = ButtonState::PRESSED;
					break;
				case 3: //Scroll Up
					if (state != GLUT_UP)
						mouseState->ScrollWheelValue++;
					break;
				case 4: //Scroll Down
					if (state != GLUT_UP)
						mouseState->ScrollWheelValue--;
					break;
				}

				mouseState->X = x;
				mouseState->Y = y;
			}
		}

		namespace Keyboard
		{
			namespace
			{
				KeyboardState* keyboardState = new KeyboardState();
			}

			KeyboardState* GetState()
			{
				return keyboardState;
;
			}

			void KeyboardPressedAction(unsigned char key,int x,int y)
			{
				Keys eKey = Keys::F1; //Set to a special key by default

				switch (key)
				{
					case 'a': case 'A': eKey = Keys::A; break;
					case 'b': case 'B': eKey = Keys::B; break;
					case 'c': case 'C': eKey = Keys::C; break;
					case 'd': case 'D': eKey = Keys::D; break;
					case 'e': case 'E': eKey = Keys::E; break;
					case 'f': case 'F': eKey = Keys::F; break;
					case 'g': case 'G': eKey = Keys::G; break;
					case 'h': case 'H': eKey = Keys::H; break;
					case 'i': case 'I': eKey = Keys::I; break;
					case 'j': case 'J': eKey = Keys::J; break;
					case 'k': case 'K': eKey = Keys::K; break;
					case 'l': case 'L': eKey = Keys::L; break;
					case 'm': case 'M': eKey = Keys::M; break;
					case 'n': case 'N': eKey = Keys::N; break;
					case 'o': case 'O': eKey = Keys::O; break;
					case 'p': case 'P': eKey = Keys::P; break;
					case 'q': case 'Q': eKey = Keys::Q; break;
					case 'r': case 'R': eKey = Keys::R; break;
					case 's': case 'S': eKey = Keys::S; break;
					case 't': case 'T': eKey = Keys::T; break;
					case 'u': case 'U': eKey = Keys::U; break;
					case 'v': case 'V': eKey = Keys::V; break;
					case 'w': case 'W': eKey = Keys::W; break;
					case 'x': case 'X': eKey = Keys::X; break;
					case 'y': case 'Y': eKey = Keys::Y; break;
					case 'z': case 'Z': eKey = Keys::Z; break;
					case '0': eKey = Keys::NUMPAD0; break;
					case '1': eKey = Keys::NUMPAD1; break;
					case '2': eKey = Keys::NUMPAD2; break;
					case '3': eKey = Keys::NUMPAD3; break;
					case '4': eKey = Keys::NUMPAD4; break;
					case '5': eKey = Keys::NUMPAD5; break;
					case '6': eKey = Keys::NUMPAD6; break;
					case '7': eKey = Keys::NUMPAD7; break;
					case '8': eKey = Keys::NUMPAD9; break;
					case '9': eKey = Keys::NUMPAD9; break;
					case '+': eKey = Keys::PLUS; break;
					case '-': eKey = Keys::SUBTRACT; break;
					case '*': eKey = Keys::MULTIPLY; break;
					case '/': eKey = Keys::DIVIDE; break;
					case '.': eKey = Keys::DECIMAL; break;
					case '=': eKey = Keys::EQUALS; break;
					case ' ': eKey = Keys::SPACE; break;
					case '\r': eKey = Keys::RETURN; break;
					case '\t': eKey = Keys::TAB; break;
					case '\b': eKey = Keys::BACKSPACE; break;
					case (char)127: eKey = Keys::DELETEKEY; break;
					case (char)27: eKey = Keys::ESCAPE; break;
				}

				if (eKey != Keys::F1)
					keyboardState->Items[eKey] = KeyState::PRESSED;
			}

			void KeyboardReleasedAction(unsigned char key,int x,int y)
			{
				Keys eKey = Keys::F1; //Set to a special key by default

				switch (key)
				{
					case 'a': case 'A': eKey = Keys::A; break;
					case 'b': case 'B': eKey = Keys::B; break;
					case 'c': case 'C': eKey = Keys::C; break;
					case 'd': case 'D': eKey = Keys::D; break;
					case 'e': case 'E': eKey = Keys::E; break;
					case 'f': case 'F': eKey = Keys::F; break;
					case 'g': case 'G': eKey = Keys::G; break;
					case 'h': case 'H': eKey = Keys::H; break;
					case 'i': case 'I': eKey = Keys::I; break;
					case 'j': case 'J': eKey = Keys::J; break;
					case 'k': case 'K': eKey = Keys::K; break;
					case 'l': case 'L': eKey = Keys::L; break;
					case 'm': case 'M': eKey = Keys::M; break;
					case 'n': case 'N': eKey = Keys::N; break;
					case 'o': case 'O': eKey = Keys::O; break;
					case 'p': case 'P': eKey = Keys::P; break;
					case 'q': case 'Q': eKey = Keys::Q; break;
					case 'r': case 'R': eKey = Keys::R; break;
					case 's': case 'S': eKey = Keys::S; break;
					case 't': case 'T': eKey = Keys::T; break;
					case 'u': case 'U': eKey = Keys::U; break;
					case 'v': case 'V': eKey = Keys::V; break;
					case 'w': case 'W': eKey = Keys::W; break;
					case 'x': case 'X': eKey = Keys::X; break;
					case 'y': case 'Y': eKey = Keys::Y; break;
					case 'z': case 'Z': eKey = Keys::Z; break;
					case '0': eKey = Keys::NUMPAD0; break;
					case '1': eKey = Keys::NUMPAD1; break;
					case '2': eKey = Keys::NUMPAD2; break;
					case '3': eKey = Keys::NUMPAD3; break;
					case '4': eKey = Keys::NUMPAD4; break;
					case '5': eKey = Keys::NUMPAD5; break;
					case '6': eKey = Keys::NUMPAD6; break;
					case '7': eKey = Keys::NUMPAD7; break;
					case '8': eKey = Keys::NUMPAD9; break;
					case '9': eKey = Keys::NUMPAD9; break;
					case '+': eKey = Keys::PLUS; break;
					case '-': eKey = Keys::SUBTRACT; break;
					case '*': eKey = Keys::MULTIPLY; break;
					case '/': eKey = Keys::DIVIDE; break;
					case '.': eKey = Keys::DECIMAL; break;
					case '=': eKey = Keys::EQUALS; break;
					case ' ': eKey = Keys::SPACE; break;
					case '\r': eKey = Keys::RETURN; break;
					case '\t': eKey = Keys::TAB; break;
					case '\b': eKey = Keys::BACKSPACE; break;
					case (char)127: eKey = Keys::DELETEKEY; break;
					case (char)27: eKey = Keys::ESCAPE; break;
				}

				if (eKey != Keys::F1)
					keyboardState->Items[eKey] = KeyState::RELEASED;

			}

			void KeyboardSpecialPressedAction(int key, int x, int y )
			{
				Keys eKey = Keys::A; //Set to a non special key by default

				switch (key)
				{
					case GLUT_KEY_F1: eKey = Keys::F1; break;
					case GLUT_KEY_F2: eKey = Keys::F2; break;
					case GLUT_KEY_F3: eKey = Keys::F3; break;
					case GLUT_KEY_F4: eKey = Keys::F4; break;
					case GLUT_KEY_F5: eKey = Keys::F5; break;
					case GLUT_KEY_F6: eKey = Keys::F6; break;
					case GLUT_KEY_F7: eKey = Keys::F7; break;
					case GLUT_KEY_F8: eKey = Keys::F8; break;
					case GLUT_KEY_F9: eKey = Keys::F9; break;
					case GLUT_KEY_F10: eKey = Keys::F10; break;
					case GLUT_KEY_F11: eKey = Keys::F11; break;
					case GLUT_KEY_F12: eKey = Keys::F12; break;
					case GLUT_KEY_LEFT: eKey = Keys::LEFT; break;
					case GLUT_KEY_UP: eKey = Keys::UP; break;
					case GLUT_KEY_RIGHT: eKey = Keys::RIGHT; break;
					case GLUT_KEY_DOWN: eKey = Keys::DOWN; break;
					case GLUT_KEY_PAGE_UP: eKey = Keys::PAGEUP; break;
					case GLUT_KEY_PAGE_DOWN: eKey = Keys::PAGEDOWN; break;
					case GLUT_KEY_HOME: eKey = Keys::HOME; break;
					case GLUT_KEY_END: eKey = Keys::END; break;
					case GLUT_KEY_INSERT: eKey = Keys::INSERT; break;
					case GLUT_KEY_SHIFT_L: eKey = Keys::LEFTSHIFT; break;
					case GLUT_KEY_SHIFT_R: eKey = Keys::RIGHTSHIFT; break;
					case GLUT_KEY_ALT_L: eKey = Keys::LEFTALT; break;
					case GLUT_KEY_ALT_R: eKey = Keys::RIGHTALT; break;
					case GLUT_KEY_CTRL_L: eKey = Keys::LEFTCONTROL; break;
					case GLUT_KEY_CTRL_R: eKey = Keys::RIGHTCONTROL; break;
				}

				if (eKey != Keys::A)
					keyboardState->Items[eKey] = KeyState::PRESSED;
			}

			void KeyboardSpecialReleasedAction(int key, int x, int y )
			{
				Keys eKey = Keys::A; //Set to a non special key by default

				switch (key)
				{
					case GLUT_KEY_F1: eKey = Keys::F1; break;
					case GLUT_KEY_F2: eKey = Keys::F2; break;
					case GLUT_KEY_F3: eKey = Keys::F3; break;
					case GLUT_KEY_F4: eKey = Keys::F4; break;
					case GLUT_KEY_F5: eKey = Keys::F5; break;
					case GLUT_KEY_F6: eKey = Keys::F6; break;
					case GLUT_KEY_F7: eKey = Keys::F7; break;
					case GLUT_KEY_F8: eKey = Keys::F8; break;
					case GLUT_KEY_F9: eKey = Keys::F9; break;
					case GLUT_KEY_F10: eKey = Keys::F10; break;
					case GLUT_KEY_F11: eKey = Keys::F11; break;
					case GLUT_KEY_F12: eKey = Keys::F12; break;
					case GLUT_KEY_LEFT: eKey = Keys::LEFT; break;
					case GLUT_KEY_UP: eKey = Keys::UP; break;
					case GLUT_KEY_RIGHT: eKey = Keys::RIGHT; break;
					case GLUT_KEY_DOWN: eKey = Keys::DOWN; break;
					case GLUT_KEY_PAGE_UP: eKey = Keys::PAGEUP; break;
					case GLUT_KEY_PAGE_DOWN: eKey = Keys::PAGEDOWN; break;
					case GLUT_KEY_HOME: eKey = Keys::HOME; break;
					case GLUT_KEY_END: eKey = Keys::END; break;
					case GLUT_KEY_INSERT: eKey = Keys::INSERT; break;
					case GLUT_KEY_SHIFT_L: eKey = Keys::LEFTSHIFT; break;
					case GLUT_KEY_SHIFT_R: eKey = Keys::RIGHTSHIFT; break;
					case GLUT_KEY_ALT_L: eKey = Keys::LEFTALT; break;
					case GLUT_KEY_ALT_R: eKey = Keys::RIGHTALT; break;
					case GLUT_KEY_CTRL_L: eKey = Keys::LEFTCONTROL; break;
					case GLUT_KEY_CTRL_R: eKey = Keys::RIGHTCONTROL; break;
				}

				if (eKey != Keys::A)
					keyboardState->Items[eKey] = KeyState::RELEASED;
			}
		}

		void Initialise()
		{
			glutPassiveMotionFunc(Mouse::PassiveMouseMotion);
			glutMotionFunc(Mouse::MouseMotion);
			glutMouseFunc(Mouse::MouseAction);
			glutKeyboardFunc(Keyboard::KeyboardPressedAction);
			glutKeyboardUpFunc(Keyboard::KeyboardReleasedAction);
			glutSpecialFunc(Keyboard::KeyboardSpecialPressedAction);
			glutSpecialUpFunc(Keyboard::KeyboardSpecialReleasedAction);
		}

		void Destroy()
		{
			glutPassiveMotionFunc(nullptr);
			glutMotionFunc(nullptr);
			glutMouseFunc(nullptr);
			glutKeyboardFunc(nullptr);
			glutKeyboardUpFunc(nullptr);
			glutSpecialFunc(nullptr);
			glutSpecialUpFunc(nullptr);
		}
	}
}